package com.projects.daniel.triviaclient;

import java.util.ArrayList;

public class MessageStruct {
    ArrayList<String> values;
    int code;

    public ArrayList<String> getValues() {
        return values;
    }

    public void setValues(ArrayList<String> values) {
        this.values = values;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public void reset() {
        this.code = 0;
        values.clear();
    }
}
