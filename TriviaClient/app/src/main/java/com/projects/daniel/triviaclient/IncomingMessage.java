package com.projects.daniel.triviaclient;

import java.util.ArrayList;

/**
 * Created by danie on 09/07/2017.
 */

public class IncomingMessage {
    public ArrayList<String> values;
    public int code;
    public IncomingMessage(int _code) { values = new ArrayList<>(); code =_code;}
    public IncomingMessage() { values = new ArrayList<>(); code =0;}
}
