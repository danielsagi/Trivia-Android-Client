package com.projects.daniel.triviaclient;

import android.content.Intent;
import android.graphics.Color;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.IOException;
import java.io.PrintWriter;

public class Game extends AppCompatActivity {
    Object lock = new Object();

    String username;
    int questionTime, questionNum;
    boolean answered, finished = false;

    int answeredIndex = -1;
    String timeToAnswerFormat;

    Button[] answersButs = new Button[4];
    TextView questionLbl, questionCountLbl, timerLbl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);

        EventBus.getDefault().register(this);

        // getting extras from caller's intent
        Intent caller = getIntent();
        username =  caller.getStringExtra("username");
        questionNum =  caller.getIntExtra("questionNum", 0);
        questionTime = caller.getIntExtra("questionTime", 0);

        // setting views on start
        questionLbl = (TextView) findViewById(R.id.questionLbl);
        questionCountLbl = (TextView) findViewById(R.id.questionCounter);
        timerLbl = (TextView) findViewById(R.id.textView14);
        answersButs[0] = (Button) findViewById(R.id.but1);
        answersButs[1] = (Button) findViewById(R.id.but2);
        answersButs[2] = (Button) findViewById(R.id.but3);
        answersButs[3] = (Button) findViewById(R.id.but4);

        questionCountLbl.setText(1 + "/" + questionNum);
        timerLbl.setText(questionTime + "");

        EventBus.getDefault().post(new GameEvents.TimerStart());

        // saving sticky question, and clearing the list of stickies
        QuestionMessage question = EventBus.getDefault().getStickyEvent(QuestionMessage.class);
        EventBus.getDefault().removeAllStickyEvents();

        // posting again the sticky question, for the handler to take care of
        EventBus.getDefault().postSticky(question);
    }

    /** Thread Handles the countdown and the basic architecture of the game*/
    @Subscribe (threadMode = ThreadMode.BACKGROUND)
    public void timerHandler(GameEvents.TimerStart timerStartE) {
        while (!finished) {
            // counting seconds, and changing the label after each second passes
            for (int i = questionTime; i > 0 && !finished; i--) {
                EventBus.getDefault().post(new GameEvents.TimerUpdate(i));
                SystemClock.sleep(1000);
            }

            if (!finished) {
                if (!answered)
                    answeredIndex = 4;
                EventBus .getDefault().post(new OutgoingMessage(Codes.ANSWER + "" + (answeredIndex + 1) + timeToAnswerFormat));
            }

            // if timer exceeded, sending wrong answer and restarting timer
//            if (!answered) {
//                EventBus.getDefault().post(new OutgoingMessage(Codes.ANSWER + "500"));
//                //EventBus.getDefault().post(new GameEvents.TimerStart());
//            }
        }
        finish();
    }

    // this event will open on UI thread, which lets us change properties
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void updateTimer(GameEvents.TimerUpdate updateTimeE) {
        timerLbl.setText(updateTimeE.newTime + "");
    }

    /**Answer Event*/
    @Subscribe
    public void answerClick(View view) {
        if (!timerLbl.getText().toString().equals("0")) {
            // iterating over buttons to check what answer index has been chosen
            for (int i = 0; i < 4; i ++) {
                if (answersButs[i].getText().toString().equals(((Button)view).getText().toString())) {
                    // after getting the index, formatting the message and sending it.
                    answersButs[i].setBackgroundColor(Color.GREEN);

                    timeToAnswerFormat = String.format("%02d", questionTime - Integer.parseInt(timerLbl.getText().toString()));
                    answeredIndex = i;
                    break;
                }
            }

            answered = true;
        }
    }

    /** Sticky Event Handler, Gets A New Question Every Time*/
    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onQuestionSend(QuestionMessage msg) {
        // setting all buttons and question to be with the values of the message
        if (answersButs[3] != null) {

            questionLbl.setText(msg.questionString);
            for (int i =0; i< 4; i++) {
                answersButs[i].setBackgroundColor(Color.GRAY);
                answersButs[i].setText(msg.answers.get(i).toString());
            }

            synchronized (EventBus.getDefault()) {
                //starting timer thread
                EventBus.getDefault().post(new GameEvents.TimerStart());
            }
            // setting answered flag to false, to check if a player has answered during runtime
        }
    }

    /** Event Handles Incoming Messages, Mainly The Answer Feedback*/
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void inMessages(IncomingMessage msg) {
        switch (msg.code) {
            case Codes.RIGHT_ANSWER:
                Toast.makeText(this, "You Are Right!", Toast.LENGTH_SHORT).show();
                for (int i = 0; i < 4; i++)
                    if (i != answeredIndex)
                        answersButs[i].setBackgroundColor(Color.RED);

                answered = false;
                break;
            case Codes.WRONG_ANSWER:
                Toast.makeText(this, "WRONG", Toast.LENGTH_SHORT).show();
                for (int i = 0; i < 4; i++)
                    answersButs[i].setBackgroundColor(Color.RED);

                answered = false;
                break;
            case Codes.GAME_END:
                Toast.makeText(this, "End Of Game", Toast.LENGTH_SHORT).show();
                finished = true;

                try {Thread.sleep(500);}
                catch (InterruptedException e){}
                finish();
                break;
        }
    }

    /////////////// GENERAL METHODS ////////////////////
    @Override
    protected void onResume() {
        super.onResume();
        if (!EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().register(this);
    }
    @Override
    protected void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        EventBus.getDefault().post(new OutgoingMessage(Codes.GAME_LEAVE + ""));
        Toast.makeText(this, "Left Game", Toast.LENGTH_SHORT).show();

        finished = true;
        try {Thread.sleep(500);}
        catch (InterruptedException e){}

        finish();
    }

    /**
     * Event Will be woken when an out message is posted by some activity
     * Thread's starting when some activity posts a message.
     * Sending that message to client in this thread (Because of Gui restrictions)
     *
     * */
    @Subscribe(threadMode = ThreadMode.ASYNC)
    public void SendMessageEvent(OutgoingMessage msgGiven) {
        try
        {
            PrintWriter pw = new PrintWriter(SocketManager.socket.getOutputStream(), true);
            pw.print(msgGiven.rawMsg);
            pw.flush();
        }
        catch (IOException e)
        {
            Log.e("Writing To Socket", msgGiven.rawMsg + "\n" +e.getMessage());
            EventBus.getDefault().post(new IncomingMessage(Codes.ERROR_SOCKET));
        }
    }
}
