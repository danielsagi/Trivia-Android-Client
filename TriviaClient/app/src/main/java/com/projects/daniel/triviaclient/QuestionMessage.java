package com.projects.daniel.triviaclient;

import java.util.ArrayList;

/**
 * Created by danie on 11/07/2017.
 */

public class QuestionMessage {
    String questionString;
    ArrayList<String> answers;

    public QuestionMessage() {
        answers = new ArrayList<>();
    }

    public QuestionMessage(String question) {
        this.questionString = question;
        answers = new ArrayList<>();
    }
}
