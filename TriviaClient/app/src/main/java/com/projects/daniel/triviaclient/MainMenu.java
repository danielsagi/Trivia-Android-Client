package com.projects.daniel.triviaclient;

import android.content.Intent;
import android.graphics.Paint;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class MainMenu extends AppCompatActivity {
    TextView usrView;
    String username;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);

        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if(extras == null) {
                username= null;
            } else {
                username= extras.getString("usernameIntent");
            }
        } else {
            username= (String) savedInstanceState.getSerializable("usernameIntent");
        }

        usrView = (TextView) findViewById(R.id.userLbl);
        usrView.setText(username);
    }

    public void createRoomClick(View view) {
        Intent create = new Intent(this.getApplicationContext(), CreateRoom.class);
        create.putExtra("username", username);
        startActivity(create);
    }

    public void joinRoomClick(View view) {
        Intent join = new Intent(this.getApplicationContext(), JoinRoom.class);
        join.putExtra("username", username);
        startActivity(join);
    }

    public void personalStatusClick(View view) {
        Intent scoresIntent = new Intent(this.getApplicationContext(), InfoActivity.class);
        scoresIntent.putExtra("TYPE", "status");
        startActivity(scoresIntent);
    }

    public void bestScoresClick(View view) {
        Intent scoresIntent = new Intent(this.getApplicationContext(), InfoActivity.class);
        scoresIntent.putExtra("TYPE", "scores");
        startActivity(scoresIntent);
    }

}
