package com.projects.daniel.triviaclient;

import android.util.Log;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.Socket;

public class SocketManager {
    static Socket socket;
    static boolean receive = true;
    static boolean sentConnectReq = false;

    static String serverIp = "192.168.1.2";
    static int serverPort = 27493;

    static Object lock = new Object();

    public SocketManager() {
        EventBus.getDefault().register(this);

        // starting the receive message thread to always wait on a message
        Thread t = new Thread(new ReceiveMessageThread());
        t.start();
    }


    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    public void ConnectEvent(ConnectMessage msg) {
        try
        {
            InetAddress serverAddr = InetAddress.getByName(serverIp);
            Log.d("ClientActivity", "C: Connecting...");
            socket = new Socket(serverAddr, serverPort);
            EventBus.getDefault().post(new IncomingMessage(Codes.CONNECTED));
        }
        catch(IOException e)
        {
            Log.e("SocketManager", "C: Error" + e.getMessage());
        }

        // resetting flag for receive message to know to send connect request next time on failure
        synchronized (lock) {
            sentConnectReq = false;
        }
    }

    /**
     * Thread Reads and Parses Messages From Socket
     * Always listening for a message, when getting one, building and sending it through the IN bus,
     * To everyone who's currently registered
     *
     * */
    private class ReceiveMessageThread implements Runnable {
        @Override
        public void run() {
            while (receive) {
                try {
                    IncomingMessage message = new IncomingMessage();
                    InputStreamReader inReader = null;

                    // opening input reader
                    try {
                        inReader = new InputStreamReader(SocketManager.socket.getInputStream());
                    } catch (IOException e) {
                        Log.e("", e.getMessage());
                    }

                    // getting message code
                    message.code = (Helper.returnIntPartFromSocket(3, inReader));

                    // if theres more data to the message, adding it here
                    switch (message.code) {
                        case Codes.ROOM_USERS_RESPONSE:
                            int length = Helper.returnIntPartFromSocket(1, inReader);
                            if (length > 0) {
                                for (int i = 0; i < length; i++) {
                                    int userLength = Helper.returnIntPartFromSocket(2, inReader);
                                    message.values.add(Helper.returnStringPartFromSocket(userLength, inReader));

                                    // Sending sticky event instead, to make sure all subscribers are getting the latest update
                                    EventBus.getDefault().postSticky(new UsersListMessage(message));
                                }
                            } else {
                                // empty message in case of failure in room
                                message.code = Codes.NO_USERS_RESPONSE;
                            }

                            break;

                        case Codes.AVAILABLE_ROOM_RESPONSE:
                            int games = Helper.returnIntPartFromSocket(4, inReader);
                            if (games > 0) {
                                for (int i = 0; i < games; i++) {
                                    message.values.add(Helper.returnStringPartFromSocket(4, inReader));
                                    int nameLen = Helper.returnIntPartFromSocket(2, inReader);
                                    message.values.add(Helper.returnStringPartFromSocket(nameLen, inReader));
                                }
                            }
                            else {
                                message.code = Codes.NO_AVAILABLE_ROOM_RESPONSE;
                            }
                            break;

                        case Codes.PERSONAL_STATUS_RESPONSE:
                            message.values.add(Helper.returnIntPartFromSocket(4, inReader) + ""); // Num Games
                            message.values.add(Helper.returnIntPartFromSocket(6, inReader) + ""); // Right Answers
                            message.values.add(Helper.returnIntPartFromSocket(6, inReader) + ""); // Wrong Answers
                            message.values.add(Helper.returnStringPartFromSocket(4, inReader)); // num games
                            break;

                        case Codes.BEST_SCORES_RESPOND:
                            // iterating over given users, and adding them to list as strings;
                            for (int i = 0; i < 3; i++) {
                                int usernameLen = Helper.returnIntPartFromSocket(2, inReader);
                                String username = Helper.returnStringPartFromSocket(usernameLen, inReader);
                                int score = Helper.returnIntPartFromSocket(6, inReader);
                                message.values.add(username);
                                message.values.add(Integer.toString(score));
                            }
                            break;

                        case Codes.ROOM_JOIN_RESPONSE:
                            message.code = Integer.parseInt(message.code + Helper.returnCharFromSocket(inReader));

                            if (message.code == Codes.ROOM_JOIN_SUCCESS) {
                                message.values.add(Helper.returnStringPartFromSocket(2, inReader)); // question num
                                message.values.add(Helper.returnStringPartFromSocket(2, inReader)); // question time
                            }
                            break;

                        case Codes.QUESTION:
                            int sizeQuestion = Helper.returnIntPartFromSocket(3, inReader);

                            if (sizeQuestion > 0) {
                                QuestionMessage msg = new QuestionMessage(Helper.returnStringPartFromSocket(sizeQuestion, inReader));
                                for (int i = 0; i < 4; i++) {
                                    int queSize = Helper.returnIntPartFromSocket(3, inReader);
                                    msg.answers.add(Helper.returnStringPartFromSocket(queSize, inReader));
                                }
                                EventBus.getDefault().postSticky(msg);
                            }
                            else
                                message.code = Codes.QUESTION_FAIL;

                            break;

                        case Codes.GAME_END:
                            int numberOfUsers = Helper.returnIntPartFromSocket(1, inReader);
                            message.values.add(numberOfUsers + ""); // number of users

                            // adding users' information
                            for (int i = 0; i < numberOfUsers; i++) {
                                int sizeOfUser = Helper.returnIntPartFromSocket(2,inReader);
                                message.values.add(Helper.returnStringPartFromSocket(sizeOfUser, inReader)); // username
                                message.values.add(Helper.returnStringPartFromSocket(2, inReader)); // score
                            }
                            break;

                        case Codes.ROOM_CLOSE_RESPONSE:
                            break;

                        // for any other code, adding "status" byte
                        default:
                            // reading following ASCII status code, converting it to string, then to Int
                            message.code = Integer.parseInt(message.code + Helper.returnCharFromSocket(inReader));
                            break;
                    }

                    /** Finally Posting To All Subscribers On The Bus The Complete Message*/
                    if (message.code != Codes.ROOM_USERS_RESPONSE)
                        EventBus.getDefault().post(message);
                }

                catch(Exception e)
                {
                    Log.e("Some Error", e.getMessage());
                    EventBus.getDefault().post(new IncomingMessage(Codes.ERROR_SOCKET));

                    // STARTING CONNECTION AGAIN, While Checking to send only once per try,
                    // the connect event will notify if failed, then set the flag to false, thus
                    // condition will be true
                    synchronized (lock) {
                        if (!sentConnectReq) {
                            sentConnectReq = true;
                            EventBus.getDefault().post(new ConnectMessage());
                        }
                    }
                }
            }
        }
    }

    @Subscribe
    public void closeReceiveEvent(StopMessage msg) {
        receive = false;
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        EventBus.getDefault().unregister(this);
    }
}