package com.projects.daniel.triviaclient;

public class Codes {
    static final int SIGN_IN_REQUEST  = 200;
    static final int SIGN_OUT_REQUEST  = 201;
    static final int SIGN_IN_SUCCESS =1020;
    static final int SIGN_IN_WRONG_DETAILS  =1021;
    static final int SIGN_IN_USER_CONNECTED  =1022;
    static final int SIGN_UP_REQUEST = 203;
    static final int SIGN_UP_SUCCESS  =1040;
    static final int SIGN_UP_PASS_ILLEGAL  =1041;
    static final int SIGN_UP_USERNAME_EXISTS  =1042;
    static final int SIGN_UP_USERNAME_ILLEGAL  =1043;
    static final int SIGN_UP_OTHER  =1044;
    static final int AVAILABLE_ROOM_REQUEST = 205;
    static final int AVAILABLE_ROOM_RESPONSE = 106;
    static final int NO_AVAILABLE_ROOM_RESPONSE = 1060;
    static final int ROOM_USERS_REQUEST = 207;
    static final int ROOM_USERS_RESPONSE = 108;
    static final int NO_USERS_RESPONSE = 1080;
    static final int ROOM_JOIN_REQUEST = 209;
    static final int ROOM_JOIN_RESPONSE = 110;
    static final int ROOM_JOIN_SUCCESS  =1100;
    static final int ROOM_JOIN_ROOM_FULL  =1101;
    static final int ROOM_JOIN_OTHER  =1102;
    static final int ROOM_LEAVE_REQUEST = 211;
    static final int ROOM_LEAVE_RESPONSE = 112;
    static final int ROOM_LEAVE_SUCCESS  =1120;
    static final int ROOM_CREATE_REQUEST = 213;
    static final int ROOM_CREATE_SUCCESS  =1140;
    static final int ROOM_CREATE_FAIL  =1141;
    static final int ROOM_CLOSE_REQUEST = 215;
    static final int ROOM_CLOSE_RESPONSE = 116;
    static final int GAME_START = 217;
    static final int QUESTION = 118;
    static final int QUESTION_FAIL = 1180;
    static final int ANSWER = 219;
    static final int RIGHT_ANSWER = 1201;
    static final int WRONG_ANSWER = 1200;

    static final int GAME_END = 121;
    static final int GAME_LEAVE = 222;
    static final int BEST_SCORES_REQUEST = 223;
    static final int BEST_SCORES_RESPOND = 124;
    static final int PERSONAL_STATUS_REQUEST = 225;
    static final int PERSONAL_STATUS_RESPONSE = 126;
    static final int EXIT_APPLICATION =299;

    static final int ERROR_SOCKET =999;
    static final int CONNECT_REQUEST =1000;
    static final int CONNECTED = 1001;
}
