package com.projects.daniel.triviaclient;

import java.util.ArrayList;

/**
 * Created by danie on 11/07/2017.
 */

public class UsersListMessage {
    public ArrayList<String> values;
    public int code;
    public UsersListMessage(IncomingMessage in) {
        this.values = in.values;
        this.code = in.code;
    }
}
