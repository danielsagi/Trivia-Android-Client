package com.projects.daniel.triviaclient;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.EventLog;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.IOException;
import java.io.PrintWriter;

public class CreateRoom extends AppCompatActivity {
    EditText nameInput = null;
    EditText playersInput = null;
    EditText questionsInput = null;
    EditText timeInput = null;

    SocketManager sockManager = new SocketManager();
    String username;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_room);

        Intent caller = getIntent();
        username = caller.getStringExtra("username");
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void inMessageEvent(IncomingMessage message) {
        if (message.code == Codes.ROOM_CREATE_SUCCESS) {
            Toast.makeText(getApplicationContext(), "Room Created", Toast.LENGTH_SHORT).show();

            // starting lobby with admin privileges
            Intent lobby = new Intent(getApplicationContext(), RoomLobby.class);
            Bundle extras = new Bundle();

            // parameters
            extras.putBoolean("isAdmin", true);
            extras.putString("username", username);
            extras.putString("roomName", nameInput.getText().toString());
            extras.putInt("questionNum", Integer.parseInt(questionsInput.getText().toString()));
            extras.putInt("questionTime", Integer.parseInt(timeInput.getText().toString()));
            extras.putInt("maxUsers", Integer.parseInt(playersInput.getText().toString()));

            lobby.putExtras(extras);
            startActivity(lobby);
            finish();
        }
        else if (message.code == Codes.ROOM_CREATE_FAIL) {
            Toast.makeText(getApplicationContext(), "Failed Creating Room, Try Again", Toast.LENGTH_SHORT).show();
        }
    }

    public void createClick(View view) {
        nameInput = (EditText) findViewById(R.id.nameEdit);
        playersInput = (EditText) findViewById(R.id.playersEdit);
        questionsInput = (EditText) findViewById(R.id.questionsEdit);
        timeInput = (EditText) findViewById(R.id.timeEdit);

        String msgToSend = Codes.ROOM_CREATE_REQUEST + "";
        msgToSend += String.format("%02d", nameInput.getText().length() );
        msgToSend += nameInput.getText();

        if (playersInput.getText().length() == 1) {
            msgToSend += playersInput.getText();
            msgToSend += String.format("%02d", Integer.parseInt(questionsInput.getText().toString()) );
            msgToSend += String.format("%02d", Integer.parseInt(timeInput.getText().toString()) );

            EventBus.getDefault().post(new OutgoingMessage(msgToSend));
        }
        else {
            Toast.makeText(getApplicationContext(), "Please Enter A Number From 1-9", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }

    /**
     * Event Will be woken when an out message is posted by some activity
     * Thread's starting when some activity posts a message.
     * Sending that message to client in this thread (Because of Gui restrictions)
     *
     * */
    @Subscribe(threadMode = ThreadMode.ASYNC)
    public void SendMessageEvent(OutgoingMessage msgGiven) {
        try
        {
            PrintWriter pw = new PrintWriter(SocketManager.socket.getOutputStream(), true);
            pw.print(msgGiven.rawMsg);
            pw.flush();
        }
        catch (IOException e)
        {
            Log.e("Writing To Socket", msgGiven.rawMsg + "\n" +e.getMessage());
            EventBus.getDefault().post(new IncomingMessage(Codes.ERROR_SOCKET));
        }
    }
}
