package com.projects.daniel.triviaclient;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.Switch;
import android.widget.Toast;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Dictionary;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class JoinRoom extends AppCompatActivity {
    private ArrayAdapter adapterRooms, adapterUsers;
    private Map<String, String> roomsInfo = new LinkedHashMap<String, String>();
    private Pair<String, String> currentSelectedRoom = Pair.create("", "");

    private boolean autoRef = true;
    private String username;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_join_room);

        EventBus.getDefault().register(this);

        Intent caller = getIntent();
        username = caller.getStringExtra("username");

        // initializing lists adapters, for insertion
        final ListView rooms = (ListView) findViewById(R.id.roomsView);
        List<String> initialList = new ArrayList<String>(); //load these
        adapterRooms = new ArrayAdapter(this, android.R.layout.simple_list_item_1, initialList);
        rooms.setAdapter(adapterRooms);

        ListView users = (ListView) findViewById(R.id.usersView);
        List<String> initialList2 = new ArrayList<String>(); //load these
        adapterUsers = new ArrayAdapter(this, android.R.layout.simple_list_item_1, initialList2);
        users.setAdapter(adapterUsers);

        EventBus.getDefault().post(new OutgoingMessage(Codes.AVAILABLE_ROOM_REQUEST + ""));

        // setting click listener for the list of the rooms
        rooms.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String roomName = (String) adapterRooms.getItem(position);

                String roomId = "0000";
                int count = 0;
                for (Map.Entry<String, String> currentRoom : roomsInfo.entrySet()) {
                    if (count == position) {
                        roomId = currentRoom.getKey();
                    }
                    count++;
                }

                currentSelectedRoom = Pair.create(roomId, roomName);

                // Updating users list on current room selection
                if (!roomName.equals("No Rooms"))
                    EventBus.getDefault().post(new OutgoingMessage(Codes.ROOM_USERS_REQUEST + currentSelectedRoom.first));
            }
        });
    }

    /** Event Handler*/
    @Subscribe (threadMode = ThreadMode.MAIN)
    public void inMessageEvent(IncomingMessage message) {
        switch(message.code) {
            case Codes.AVAILABLE_ROOM_RESPONSE:
                adapterRooms.clear();
                roomsInfo.clear();

                String currentId = "";
                for (int i = 0; i < message.values.size(); i++) {
                    if (i % 2 != 0) {
                        String roomName = message.values.get(i);
                        adapterRooms.add(roomName);
                        roomsInfo.put(currentId, roomName);
                    }
                    else {
                        currentId = message.values.get(i);
                    }
                }
                break;

            case Codes.NO_AVAILABLE_ROOM_RESPONSE:
                adapterRooms.clear();
                adapterRooms.add("No Rooms");
                break;

            case Codes.NO_USERS_RESPONSE:
                adapterUsers.clear();
                adapterUsers.add("Room Does Not Exist, Refresh Please");
                break;
            case Codes.ROOM_JOIN_SUCCESS:
                // Starting lobby with user privileges
                Intent lobby = new Intent(getApplicationContext(), RoomLobby.class);
                Bundle extras = new Bundle();

                // parameters
                extras.putBoolean("isAdmin", false);
                extras.putString("username", username);
                extras.putString("roomName", currentSelectedRoom.second);
                extras.putInt("questionNum", Integer.parseInt(message.values.get(0)));
                extras.putInt("questionTime", Integer.parseInt(message.values.get(1)));

                lobby.putExtras(extras);
                lobby.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(lobby);

                finish();
                break;
            case Codes.ROOM_JOIN_ROOM_FULL:
                Toast.makeText(this, "Room Full, Sorry", Toast.LENGTH_SHORT).show();
                break;

            case Codes.ROOM_JOIN_OTHER:
                Toast.makeText(this, "Cannot Join Room, Please Refresh", Toast.LENGTH_SHORT).show();
                break;
        }
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onUsersList(UsersListMessage msg) {
        adapterUsers.clear();
        adapterUsers.addAll(msg.values);
        EventBus.getDefault().removeStickyEvent(msg);
    }

    public void refreshClick(View view) {
        // clearing
        currentSelectedRoom = Pair.create("", "");
        adapterUsers.clear();

        // Updating rooms list
        EventBus.getDefault().post(new OutgoingMessage(Codes.AVAILABLE_ROOM_REQUEST + ""));
    }

    public void joinClick(View view) {
        // if list contains some rooms, and user selected some room.
        if (adapterRooms.getItem(0) != "No Rooms" && currentSelectedRoom.second != "") {

            // Join request with last selected room id
            EventBus.getDefault().post(new OutgoingMessage(Codes.ROOM_JOIN_REQUEST + currentSelectedRoom.first));
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().register(this);
    }

    /**
     * Event Will be woken when an out message is posted by some activity
     * Thread's starting when some activity posts a message.
     * Sending that message to client in this thread (Because of Gui restrictions)
     *
     * */
    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    public void SendMessageEvent(OutgoingMessage msgGiven) {
        try
        {
            PrintWriter pw = new PrintWriter(SocketManager.socket.getOutputStream(), true);
            pw.print(msgGiven.rawMsg);
            pw.flush();
        }
        catch (IOException e)
        {
            Log.e("Writing To Socket", msgGiven.rawMsg + "\n" +e.getMessage());
            EventBus.getDefault().post(new IncomingMessage(Codes.ERROR_SOCKET));
        }
    }
}
