package com.projects.daniel.triviaclient;

import android.content.ComponentCallbacks2;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    private TextView label;
    private EditText userEdit;
    private EditText passEdit;

    private String username;

    SocketManager sockManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        sockManager = new SocketManager();
    }

    /** Each Time A Person Lands On The Sign In Page, Send Disconnet Automatically, To Be Safe*/
    @Override
    protected void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);

        Log.d("", new OutgoingMessage(Codes.SIGN_OUT_REQUEST + "").rawMsg);
        EventBus.getDefault().post(new OutgoingMessage(Codes.SIGN_OUT_REQUEST + ""));
    }

    protected void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    /** Event Handles Incoming Messages, Gets Woken By The Socket Manager's Receive Thread */
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void inMessageEvent(IncomingMessage message) {
        switch (message.code) {
            case Codes.SIGN_IN_SUCCESS:
                myToast("Success");
                Intent intent = new Intent(getApplicationContext(), MainMenu.class);
                intent.putExtra("usernameIntent", username);
                startActivity(intent);
                break;
            case Codes.SIGN_IN_WRONG_DETAILS:
                myToast("Wrong Details");
                break;
            case Codes.SIGN_IN_USER_CONNECTED:
                myToast("User Is Already Connected");
                break;
            case Codes.ERROR_SOCKET:
                myToast("Cannot Connect To Server, Please Wait..");
                break;
            case Codes.CONNECTED:
                myToast("Connected!");
                break;
            default:
                myToast("Unknown Result");
        }
    }

    /* User Wants To Sign In With Information Inputted In Fields */
    public void signInClick(View view)
    {
        ArrayList<EditText> edits = new ArrayList<>();
        label = (TextView) findViewById(R.id.textView4);
        edits.add((EditText) findViewById(R.id.userInput));
        edits.add((EditText) findViewById(R.id.passInput));

        username = edits.get(0).getText().toString();

        // formatting the message and posting it
        String msgStr = Codes.SIGN_IN_REQUEST + Helper.formatMessageDataFromViews(edits);
        EventBus.getDefault().post(new OutgoingMessage(msgStr));
    }

    public void registerClick(View view) {
        Intent intent = new Intent(this.getApplicationContext(), RegisterActivity.class);
        startActivity(intent);
    }

    private void myToast(String msg) {
        final String _msg = msg;
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(getApplicationContext(), _msg, Toast.LENGTH_SHORT).show();
            }
        });
    }


    /**
     * Event Will be woken when an out message is posted by some activity
     * Thread's starting when some activity posts a message.
     * Sending that message to client in this thread (Because of Gui restrictions)
     *
     * */
    @Subscribe(threadMode = ThreadMode.ASYNC)
    public void SendMessageEvent(OutgoingMessage msgGiven) {
        try
        {
            PrintWriter pw = new PrintWriter(SocketManager.socket.getOutputStream(), true);
            pw.print(msgGiven.rawMsg);
            pw.flush();
        }
        catch (IOException e)
        {
            Log.e("Writing To Socket", msgGiven.rawMsg + "\n" +e.getMessage());
            EventBus.getDefault().post(new IncomingMessage(Codes.ERROR_SOCKET));
        }
    }
}
