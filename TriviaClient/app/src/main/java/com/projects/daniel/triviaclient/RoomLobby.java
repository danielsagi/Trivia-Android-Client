package com.projects.daniel.triviaclient;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

public class RoomLobby extends AppCompatActivity {
    Button startBtn;
    TextView numView, timeView, maxView, maxTitleView, nameView;
    SocketManager sockManager = new SocketManager();
    private ArrayAdapter adapter;

    boolean isListen = true;
    boolean finished = false;

    boolean admin = false;
    String username, roomName;
    int timeForQuestion, questionNum, maxUsers;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_room_lobby);

        // getting values from caller activity. CreateRoom OR JoinRoom
        Intent caller = getIntent();
        Bundle extras = caller.getExtras();
        admin = extras.getBoolean("isAdmin");
        username = extras.getString("username");
        roomName = extras.getString("roomName");
        questionNum = extras.getInt("questionNum");
        timeForQuestion = extras.getInt("questionTime");

        // initializing views
        startBtn = (Button) findViewById(R.id.startBtnView);
        nameView = (TextView) findViewById(R.id.nameLbl);
        numView = (TextView) findViewById(R.id.questionLbl);
        timeView = (TextView) findViewById(R.id.timeLbl);
        maxView = (TextView) findViewById(R.id.maxLbl);
        maxTitleView = (TextView) findViewById(R.id.textView16);

        // setting texts at start
        numView.setText(questionNum + "");
        timeView.setText(timeForQuestion + "");
        nameView.setText(roomName);

        // instantiating adapter for use with list
        ListView lv = (ListView) findViewById(R.id.roomsView);
        List<String> initialList = new ArrayList<String>(); //load these
        adapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, initialList);
        lv.setAdapter(adapter);


        if (admin) {
            maxUsers = extras.getInt("maxUsers");
            maxView.setText(maxUsers + "");

            startBtn.setVisibility(View.VISIBLE);
            maxView.setVisibility(View.VISIBLE);
            maxTitleView.setVisibility(View.VISIBLE);

            // initializing the list on creation
            adapter.add(username + " - You - Admin");
        }

        UsersListMessage message = EventBus.getDefault().getStickyEvent(UsersListMessage.class);
        if (message != null) {
            adapter.clear();
            for (int i = 0; i < message.values.size(); i++) {
                String name = message.values.get(i);
                if (name.equals(username))
                    name += " - You";
                if (i == 0)
                    name += " - Admin";
                adapter.add(name);
            }
            EventBus.getDefault().removeAllStickyEvents();
        }
    }

    /** Event is raised when a message got sent from server*/
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void inMessage(IncomingMessage message)
    {
        switch (message.code) {
            case Codes.ROOM_LEAVE_SUCCESS:
                Toast.makeText(this, "Left Room", Toast.LENGTH_SHORT).show();
                EventBus.getDefault().unregister(this);
                finish();
                break;

            case Codes.ROOM_CLOSE_RESPONSE:
                if (admin)
                    Toast.makeText(this, "Room Deleted Successfully", Toast.LENGTH_SHORT).show();
                else
                    Toast.makeText(this, "Room Deleted By Admin", Toast.LENGTH_LONG).show();
                finish();
                break;

            // failed start game, only on admin
            case Codes.QUESTION_FAIL:
                if (admin) {
                    Toast.makeText(this, "Failed To Start Game, Try Again", Toast.LENGTH_SHORT).show();
                }
        }
    }

    /** Event Will Start Game, Keep in Mind, msg is Sticky,
     *  It Can Be Accessed Later From Game Activity
     * */
    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void startGame(QuestionMessage msg) {
        Intent game = new Intent(getApplicationContext(), Game.class);
        game.putExtra("username", username);
        game.putExtra("questionNum", questionNum);
        game.putExtra("questionTime", timeForQuestion);
        startActivity(game);
        finish();
    }

    /** Event is raised even if the message was sent before registration,
     *  For Making Sure We Handle The Message. Also Keeps Up To Date
     * */
    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onUsersList(UsersListMessage msg) {
        adapter.clear();
        for (int i = 0; i < msg.values.size(); i++) {
            String name = msg.values.get(i);
            if (i == 0)
                name += " - Admin";
            if (name.equals(username))
                name += " - You";
            adapter.add(name);
        }
        EventBus.getDefault().removeAllStickyEvents();
    }

    public void startGameClick(View view) {
        EventBus.getDefault().post(new OutgoingMessage(Codes.GAME_START + ""));
    }

    @Override
    protected void onResume() {
        // registering for getting notifications of users list updates
        super.onResume();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onBackPressed() {
        //if admin quits, closes room. if user quits, leaves room
        if (admin)
            EventBus.getDefault().post(new OutgoingMessage(Codes.ROOM_CLOSE_REQUEST + ""));
        else
            EventBus.getDefault().post(new OutgoingMessage(Codes.ROOM_LEAVE_REQUEST + ""));
    }

    /**
     * Event Will be woken when an out message is posted by some activity
     * Thread's starting when some activity posts a message.
     * Sending that message to client in this thread (Because of Gui restrictions)
     *
     * */
    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    public void SendMessageEvent(OutgoingMessage msgGiven) {
        try
        {
            PrintWriter pw = new PrintWriter(SocketManager.socket.getOutputStream(), true);
            pw.print(msgGiven.rawMsg);
            pw.flush();
        }
        catch (IOException e)
        {
            Log.e("Writing To Socket", msgGiven.rawMsg + "\n" +e.getMessage());
            EventBus.getDefault().post(new IncomingMessage(Codes.ERROR_SOCKET));
        }
    }
}
