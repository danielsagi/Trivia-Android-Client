package com.projects.daniel.triviaclient;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

public class InfoActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        EventBus.getDefault().register(this);

        Intent caller = getIntent();
        String type = caller.getStringExtra("TYPE");

        if (type.equals("scores")) {
            EventBus.getDefault().post(new OutgoingMessage(Codes.BEST_SCORES_REQUEST + ""));
        }
        else if (type.equals("status")) {
            EventBus.getDefault().post(new OutgoingMessage(Codes.PERSONAL_STATUS_REQUEST + ""));
        }
    }

    /** Event Handles Incoming Messages, Gets Woken By The Socket Manager's Receive Thread */
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void inMessageEvent(IncomingMessage message) {
        if (message.code == Codes.PERSONAL_STATUS_RESPONSE) {
            displayStatus(message);
        }
        else if (message.code == Codes.BEST_SCORES_RESPOND){
            displayScores(message);
        }
        EventBus.getDefault().unregister(this);
    }

    private void displayStatus(IncomingMessage message) {
        setContentView(R.layout.activity_status);

        TextView[] views = new TextView[4];
        views[0] = (TextView) findViewById(R.id.user1Lbl);
        views[1] = (TextView) findViewById(R.id.user2Lbl);
        views[2] = (TextView) findViewById(R.id.user3Lbl);
        views[3] = (TextView) findViewById(R.id.user4Lbl);

        // iterating over items in array, in form of: USERNAME, SCORES
        for (int i = 0; i < 3; i++) {
            String str = message.values.get(i);
            views[i].setText(str);
        }

        views[3].setText(message.values.get(3).substring(0,2) + "." + message.values.get(3).substring(2));
    }

    private void displayScores(IncomingMessage message) {
        setContentView(R.layout.activity_scores);

        TextView[] views = new TextView[3];
        views[0] = (TextView) findViewById(R.id.user1Lbl);
        views[1] = (TextView) findViewById(R.id.user2Lbl);
        views[2] = (TextView) findViewById(R.id.user3Lbl);

        // iterating over items in array, in form of: USERNAME, SCORES
        int count = 0;
        for (int i = 0; i < 5; i += 2) {
            String str = message.values.get(i) + " : " + message.values.get(i+1);
            views[count++].setText(str);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().register(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    /**
     * Event Will be woken when an out message is posted by some activity
     * Thread's starting when some activity posts a message.
     * Sending that message to client in this thread (Because of Gui restrictions)
     *
     * */
    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    public void SendMessageEvent(OutgoingMessage msgGiven) {
        try
        {
            PrintWriter pw = new PrintWriter(SocketManager.socket.getOutputStream(), true);
            pw.print(msgGiven.rawMsg);
            pw.flush();
        }
        catch (IOException e)
        {
            Log.e("Writing To Socket", msgGiven.rawMsg + "\n" +e.getMessage());
            EventBus.getDefault().post(new IncomingMessage(Codes.ERROR_SOCKET));
        }
    }
}
