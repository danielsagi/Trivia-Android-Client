package com.projects.daniel.triviaclient;

import android.util.Log;
import android.view.View;
import android.widget.EditText;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.Socket;
import java.util.ArrayList;


public class Helper {
public Helper() {}

    public static String formatMessageDataFromViews(ArrayList<EditText> editTexts) {
        String ret = "";
        for (int i = 0; i < editTexts.size(); i++) {
            String data = editTexts.get(i).getText().toString();
            ret += String.format("%02d", data.length()) + data;
        }
        return ret;
    }

    public static String returnStringPartFromSocket(int length, InputStreamReader inReader) {
        char[] buf = new char[length];

        if (length > 0) {
            try {
                inReader.read(buf, 0, length);
            }
            catch (IOException e) {
                Log.e("" , e.getMessage());
            }
        }

        return new String(buf);
    }

    public static int returnIntPartFromSocket(int length, InputStreamReader inReader) {
        char[] buf = new char[length];

        if (length > 0) {
            try {
                inReader.read(buf, 0, length);
            }
            catch (IOException e) {
                Log.e("" , e.getMessage());
            }
        }


        return Integer.parseInt(new String(buf));
    }

    public static String returnCharFromSocket(InputStreamReader inReader) {
        char buf = ' ';
        try {
            buf = (char) inReader.read();
        }
        catch (IOException e) {
            Log.e("" , e.getMessage());
        }

        return Character.toString((char) buf);
    }

}
