package com.projects.daniel.triviaclient;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

public class RegisterActivity extends AppCompatActivity {
    private EditText name, pass, email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
    }

    @Override
    protected void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }

    @Subscribe
    public void inMessageEvent(IncomingMessage message) {
        switch(message.code) {
            case Codes.SIGN_UP_SUCCESS:
                myToast("Please Log In");
                finish();
                break;
            case Codes.SIGN_UP_PASS_ILLEGAL:
                myToast("Pass Illegal");
                break;
            case Codes.SIGN_UP_USERNAME_EXISTS:
                myToast("Username Already Exists");
                break;
            case Codes.SIGN_UP_USERNAME_ILLEGAL:
                myToast("Username Illegal");
                break;
            default:
                break;
        }
    }

    /** Event, Sends a Sign Up Request to the server */
    public void finishClick(View view) {
        ArrayList<EditText> edits = new ArrayList<>();
        edits.add((EditText) findViewById(R.id.editText));
        edits.add((EditText) findViewById(R.id.editText2));
        edits.add((EditText) findViewById(R.id.editText3));

        // sending full message with data from fields
        String msgToSend = Codes.SIGN_UP_REQUEST + Helper.formatMessageDataFromViews(edits);
        EventBus.getDefault().post(new OutgoingMessage(msgToSend));
    }

    private void myToast(String msg) {
        final String _msg = msg;
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(getApplicationContext(), _msg, Toast.LENGTH_SHORT).show();
            }
        });
    }

    /**
     * Event Will be woken when an out message is posted by some activity
     * Thread's starting when some activity posts a message.
     * Sending that message to client in this thread (Because of Gui restrictions)
     *
     * */
    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    public void SendMessageEvent(OutgoingMessage msgGiven) {
        try
        {
            PrintWriter pw = new PrintWriter(SocketManager.socket.getOutputStream(), true);
            pw.print(msgGiven.rawMsg);
            pw.flush();
        }
        catch (IOException e)
        {
            Log.e("Writing To Socket", msgGiven.rawMsg + "\n" +e.getMessage());
            EventBus.getDefault().post(new IncomingMessage(Codes.ERROR_SOCKET));
        }
    }
}
