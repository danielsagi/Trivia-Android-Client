package com.projects.daniel.triviaclient;

import java.util.ArrayList;

/**
 * Created by danie on 09/07/2017.
 */

public class OutgoingMessage {
    public String rawMsg;

    public OutgoingMessage(String msg) { rawMsg = msg;}
    public OutgoingMessage() { rawMsg = "";}
}
